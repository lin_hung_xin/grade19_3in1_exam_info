### 一、SQL相关

1. 常用数据库

    + PostgreSQL
    + Mysql
    + Oracle
    + Sqlserver
    + Sqlite

2. SQL是什么？

    + SQL是结构化查询语言的缩写
    + SQL语言关键字不区分大小写！！！但是，针对不同的数据库，对于表名和列名，有的数据库区分大小写，有的数据库不区分大小写。同一个数据库，有的在Linux上区分大小写，有的在Windows上不区分大小写

3. SQL操作数据库的几种能力：

    + DDL：Data Definition Language，DDL允许用户定义数据，也就是创建表、删除表、修改表结构这些操作
    + DML：Data Manipulation Language，DML为用户提供添加、删除、更新数据的能力
    + DQL：Data Query Language，DQL允许用户查询数据

4. 主键

    + 主键是关系表中记录的唯一标识。主键的选取非常重要：主键不要带有业务含义，而应该使用BIGINT自增或者GUID类型。主键也不应该允许NULL

5. 外键 

    + 通过外键可以实现一对多、多对多和一对一的关系。外键既可以通过数据库来约束，也可以不设置约束，仅依靠应用程序的逻辑来保证。

<del>
6. 索引

    + 通过对数据库表创建索引，可以提高查询速度。
    + 通过创建唯一索引，可以保证某一列的值具有唯一性。
    + 数据库索引对于用户和应用程序来说都是透明的。
</del>

7. 查询

    + 基本查询 select * from <表名>
    + 条件查询 通过WHERE条件查询，可以筛选出符合指定条件的记录，而不是整个表的所有记录
    + 投影查询 使用SELECT * 表示查询表的所有列，使用SELECT 列1, 列2, 列3则可以仅返回指定列，这种操作称为投影
    + 排序查询 使用ORDER BY可以对结果集进行排序
    + 分页查询 使用这样的形式进行分页查询 offset 0 rows fetch next 3 rows only
    + 聚合查询 使用SQL提供的聚合查询，我们可以方便地计算总数、合计值、平均值、最大值和最小值
    + 多表查询 使用多表查询可以获取M x N行记录
    + 连接查询 JOIN查询需要先确定主表，然后把另一个表的数据“附加”到结果集上

8. 增删改

    + 增 insert into <表名> (列1,列2,列3) values (值1,值2,值3)
    + 删 delete from <表名> where 列1='' 
    + 改 update <表名> set 列1=XX,列2=YY where ...

9. 建库和建表（SqlServer下约定：数据库名、表名、字段名首字母大写）

    + 建库 create database <数据库名> 
    + 建表 create table <数据表名>



### 二、面向对象（C#语言）

1. 基本数据类型

    + 数据类型分为值类型和引用类型
    + 值类型包括整型、浮点型、字符型、布尔型、枚举型等
    + 类、接口、数组、委托、字符串等

2. 运算符
    + 算术运算符 包括加法、减法、乘法、除法等
    + 逻辑运算符 逻辑运算符主要包括与、或、非等，用于多个布尔型表达式之间的运算
    + 比较运算符 包括大于、小于、不等于、大于等于、小于等于等
    + 位运算符 包括与、或、 非、左移、右移等
    + 三元运算符 布尔表达式 ? 表达式 1: 表达式 2
    + 赋值运算符 最常见的是等号
    
3. 变量和常量

    + 变量的定义 数据类型  变量名
    + 常量的定义 const 数据类型 常量名 = 值;

4. 命名规范

    + Pascal 命名法（帕斯卡命名法）,指每个单词的首字母大写，适用于 类、接口、方法的命名（接口一般使用大写I开头、方法使用动词）
    + Camel 命名法（驼峰命名法）,指第一个单词小写，从第二个单词开始每个单词的首字母大写，适用于变量的命名

5. if else语句

    + 单一条件的 if 语句
        ```
        if(布尔表达式)
        {
            语句块;
        }
        ```

    + 二选一条件的 if 语句
        ```
        if(布尔表达式)
        {
            语句块 1;
        }else{
            语句块 2;
        }
        ```
    + 多选一条件的 if 语句
        ```
        if(布尔表达式 1)
        {
            语句块 1;
        }else if(布尔表达式 2){
            语句块 2;
        }
        ...
        else{
            语句块 n;
        }
        ```
    
6. for循环

    ```
    for(表达式 1; 表达式 2; 表达式3)
    {
        表达式 4;
    }
    ```

7. 类的定义

    ```
    类的访问修饰符    修饰符    类名
    {
        类的成员
    }
    ```
    > 类的访问修饰符：用于设定对类的访问限制，包括 public、internal 或者不写，用 internal 或者不写时代表只能在当前项目中访问类；public 则代表可以在任何项目中访问类。
    > 修饰符：修饰符是对类本身特点的描述，包括 abstract、sealed 和 static。abstract 是抽象的意思，使用它修饰符的类不能被实例化；sealed 修饰的类是密封类，不能被继承；static 修饰的类是静态类，不能被实例化。

8. （类中成员的）访问修饰符、修饰符

    + 访问修饰符，有4个，分别为：public、private、internal、protected
    + 修饰符，有2个，readonly （只读）和static （静态的）


9. 方法的定义
    
    ```
    访问修饰符    修饰符    返回值类型    方法名(参数列表)
    {
        语句块;
    }
    ```

10. ADO.Net

    + SqlConnection 类 连接类
    + SqlCommand类 命令类
    + SqlDataReader 读取器类
    + DataSet类 数据集（类似数据库）
    + DataTable类 数据表（类似数据库中的表）
    + SqlDataAdapter 用于查询数据并填充到DataSet或DataTable中


### 三、Winform


    无内容
